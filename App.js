import React, { Component, useEffect, useState, ScrollView } from "react";
import { StyleSheet, FlatList, View, Text } from "react-native";
import contactos from "./contactos.json";
import {
  Provider as PaperProvider,
  TextInput,
  Headline,
  IconButton,
  Surface,
} from "react-native-paper";

// const contactos = require('./contactos.json');
// const contactos = useDatos();

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { contactos: {} };
  }

  componentDidMount() {
    this.setContactos(contactos);
  }

  render() {
    return (
      <View style={styles.container}>
        <Headline style={styles.headline}> Agenda React!</Headline>
        <Text style={styles.headline}>{contactos.nombre}</Text>
        {/* <ScrollView style={styles.container}>         
            <Text datos={contacto.nombre} />      
        </ScrollView> */}

        <FlatList
          data={this.state.contactos}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <View>
              <Text>Name: {item.nombre}</Text>
              <Text>Email: {item.email}</Text>
            </View>
          )}
        />
      </View>
    );
  }
  setContactos = (contacto) => {
    debugger;
    console.log(contacto);
    this.setState({ contactos: contacto });
  };
}
// function useDatos() {
//

//   return contactos;
// }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
